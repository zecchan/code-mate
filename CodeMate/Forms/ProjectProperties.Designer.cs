﻿namespace CodeMate.Forms
{
    partial class ProjectProperties
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("Project");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("Sources");
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("Build Events");
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("Compile", new System.Windows.Forms.TreeNode[] {
            treeNode2,
            treeNode3});
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProjectProperties));
            this.panel1 = new System.Windows.Forms.Panel();
            this.tvProps = new System.Windows.Forms.TreeView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.paneSources = new CodeMate.Forms.Panes.PPSourcesPane();
            this.paneProject = new CodeMate.Forms.Panes.PPProjectPane();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.paneCompile = new CodeMate.Forms.Panes.PPCompilePane();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkGray;
            this.panel1.Controls.Add(this.tvProps);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(12, 12, 6, 12);
            this.panel1.Size = new System.Drawing.Size(274, 584);
            this.panel1.TabIndex = 0;
            // 
            // tvProps
            // 
            this.tvProps.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tvProps.Location = new System.Drawing.Point(12, 12);
            this.tvProps.Name = "tvProps";
            treeNode1.Name = "ndProject";
            treeNode1.Tag = "project";
            treeNode1.Text = "Project";
            treeNode2.Name = "ndSources";
            treeNode2.Tag = "sources";
            treeNode2.Text = "Sources";
            treeNode3.Name = "ndEvents";
            treeNode3.Tag = "build";
            treeNode3.Text = "Build Events";
            treeNode4.Name = "ndCompile";
            treeNode4.Tag = "compile";
            treeNode4.Text = "Compile";
            this.tvProps.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode4});
            this.tvProps.Size = new System.Drawing.Size(256, 560);
            this.tvProps.TabIndex = 0;
            this.tvProps.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.tvProps_AfterSelect);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.DarkGray;
            this.panel2.Controls.Add(this.panel3);
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(274, 0);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(6, 12, 12, 12);
            this.panel2.Size = new System.Drawing.Size(633, 584);
            this.panel2.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.Controls.Add(this.paneCompile);
            this.panel3.Controls.Add(this.paneSources);
            this.panel3.Controls.Add(this.paneProject);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(6, 12);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(615, 527);
            this.panel3.TabIndex = 0;
            // 
            // paneSources
            // 
            this.paneSources.BackColor = System.Drawing.Color.White;
            this.paneSources.FileFilter = null;
            this.paneSources.Location = new System.Drawing.Point(20, 113);
            this.paneSources.Name = "paneSources";
            this.paneSources.Size = new System.Drawing.Size(223, 115);
            this.paneSources.Sources = ((System.Collections.Generic.List<string>)(resources.GetObject("paneSources.Sources")));
            this.paneSources.TabIndex = 1;
            // 
            // paneProject
            // 
            this.paneProject.BackColor = System.Drawing.Color.White;
            this.paneProject.Location = new System.Drawing.Point(20, 17);
            this.paneProject.Name = "paneProject";
            this.paneProject.Size = new System.Drawing.Size(407, 74);
            this.paneProject.TabIndex = 0;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.btnCancel);
            this.panel4.Controls.Add(this.btnSave);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.Location = new System.Drawing.Point(6, 539);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(615, 33);
            this.panel4.TabIndex = 1;
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(526, 5);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(88, 28);
            this.btnCancel.TabIndex = 1;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(432, 5);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(88, 28);
            this.btnSave.TabIndex = 0;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // paneCompile
            // 
            this.paneCompile.BackColor = System.Drawing.Color.White;
            this.paneCompile.CompilerGuid = null;
            this.paneCompile.Location = new System.Drawing.Point(77, 247);
            this.paneCompile.Name = "paneCompile";
            this.paneCompile.Size = new System.Drawing.Size(397, 73);
            this.paneCompile.TabIndex = 2;
            // 
            // ProjectProperties
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(907, 584);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(600, 400);
            this.Name = "ProjectProperties";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Project Properties";
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TreeView tvProps;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnSave;
        private Panes.PPProjectPane paneProject;
        private Panes.PPSourcesPane paneSources;
        private Panes.PPCompilePane paneCompile;
    }
}