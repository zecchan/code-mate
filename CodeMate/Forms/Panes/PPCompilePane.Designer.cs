﻿namespace CodeMate.Forms.Panes
{
    partial class PPCompilePane
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.cbCompiler = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Compiler";
            // 
            // cbCompiler
            // 
            this.cbCompiler.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCompiler.FormattingEnabled = true;
            this.cbCompiler.Location = new System.Drawing.Point(102, 16);
            this.cbCompiler.Name = "cbCompiler";
            this.cbCompiler.Size = new System.Drawing.Size(384, 21);
            this.cbCompiler.TabIndex = 4;
            // 
            // PPCompilePane
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.cbCompiler);
            this.Controls.Add(this.label1);
            this.Name = "PPCompilePane";
            this.Size = new System.Drawing.Size(503, 173);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbCompiler;
    }
}
