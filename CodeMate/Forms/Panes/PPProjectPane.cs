﻿using Microsoft.WindowsAPICodePack.Dialogs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CodeMate.Forms.Panes
{
    public partial class PPProjectPane : UserControl
    {
        public PPProjectPane()
        {
            InitializeComponent();
        }

        private void btnBrowse_Click(object sender, EventArgs e)
        {
            CommonOpenFileDialog fbd = new CommonOpenFileDialog();
            fbd.IsFolderPicker = true;
            fbd.Title = "Select output path for the compilation";
            if (fbd.ShowDialog() == CommonFileDialogResult.Ok)
            {
                txOutput.Text = fbd.FileName;
            }
        }
    }
}
