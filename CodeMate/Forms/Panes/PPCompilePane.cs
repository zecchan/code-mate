﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CodeMate.Forms.Panes
{
    public partial class PPCompilePane : UserControl
    {
        List<CompilerItem> _map = new List<CompilerItem>();
        public PPCompilePane()
        {
            InitializeComponent();

            foreach(var c in GlobalStatic.AssemblyLibrary.GetCompilers())
            {
                var ci = new CompilerItem(c);
                cbCompiler.Items.Add(ci);
                _map.Add(ci);
            }
        }

        public Guid? CompilerGuid
        {
            get => (cbCompiler.SelectedItem as CompilerItem)?.Guid;
            set
            {
                if (value != null)
                {
                    var ci = _map.Where(x => x.Guid == value.Value).FirstOrDefault();
                    cbCompiler.SelectedItem = ci;
                }
                else
                {
                    cbCompiler.SelectedIndex = -1;
                }
            }
        }

        public class CompilerItem
        {
            public Guid Guid { get; set; }
            public string Name { get; set; }
            public CompilerItem(ICompiler compiler)
            {
                Guid = compiler.Guid;
                Name = compiler.Name;
            }

            public override string ToString()
            {
                return Name;
            }
        }
    }
}
