﻿namespace CodeMate.Forms.Panes
{
    partial class PPProjectPane
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txName = new System.Windows.Forms.TextBox();
            this.txVersion = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txOutput = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.cxAutoCompile = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(15, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Name";
            // 
            // txName
            // 
            this.txName.Location = new System.Drawing.Point(98, 14);
            this.txName.Name = "txName";
            this.txName.Size = new System.Drawing.Size(302, 20);
            this.txName.TabIndex = 1;
            // 
            // txVersion
            // 
            this.txVersion.Location = new System.Drawing.Point(98, 40);
            this.txVersion.Name = "txVersion";
            this.txVersion.Size = new System.Drawing.Size(83, 20);
            this.txVersion.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Version";
            // 
            // txOutput
            // 
            this.txOutput.Location = new System.Drawing.Point(98, 66);
            this.txOutput.Name = "txOutput";
            this.txOutput.ReadOnly = true;
            this.txOutput.Size = new System.Drawing.Size(302, 20);
            this.txOutput.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 69);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Output Path";
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(406, 66);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(27, 23);
            this.btnBrowse.TabIndex = 6;
            this.btnBrowse.Text = "...";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 95);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Auto Compile";
            // 
            // cxAutoCompile
            // 
            this.cxAutoCompile.AutoSize = true;
            this.cxAutoCompile.Location = new System.Drawing.Point(98, 94);
            this.cxAutoCompile.Name = "cxAutoCompile";
            this.cxAutoCompile.Size = new System.Drawing.Size(172, 17);
            this.cxAutoCompile.TabIndex = 9;
            this.cxAutoCompile.Text = "Compile when source changed";
            this.cxAutoCompile.UseVisualStyleBackColor = true;
            // 
            // PPProjectPane
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.cxAutoCompile);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnBrowse);
            this.Controls.Add(this.txOutput);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txVersion);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txName);
            this.Controls.Add(this.label1);
            this.Name = "PPProjectPane";
            this.Size = new System.Drawing.Size(505, 150);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.TextBox txName;
        public System.Windows.Forms.TextBox txVersion;
        public System.Windows.Forms.TextBox txOutput;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnBrowse;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.CheckBox cxAutoCompile;
    }
}
