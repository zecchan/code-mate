﻿namespace CodeMate.Forms.Panes
{
    partial class PPSourcesPane
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnAddFile = new System.Windows.Forms.Button();
            this.btnAddPath = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lbSources = new System.Windows.Forms.ListBox();
            this.ofd = new System.Windows.Forms.OpenFileDialog();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnAddPath);
            this.panel1.Controls.Add(this.btnAddFile);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(407, 36);
            this.panel1.TabIndex = 0;
            // 
            // btnAddFile
            // 
            this.btnAddFile.Location = new System.Drawing.Point(12, 7);
            this.btnAddFile.Name = "btnAddFile";
            this.btnAddFile.Size = new System.Drawing.Size(75, 23);
            this.btnAddFile.TabIndex = 0;
            this.btnAddFile.Text = "Add File";
            this.btnAddFile.UseVisualStyleBackColor = true;
            this.btnAddFile.Click += new System.EventHandler(this.btnAddFile_Click);
            // 
            // btnAddPath
            // 
            this.btnAddPath.Location = new System.Drawing.Point(93, 7);
            this.btnAddPath.Name = "btnAddPath";
            this.btnAddPath.Size = new System.Drawing.Size(75, 23);
            this.btnAddPath.TabIndex = 1;
            this.btnAddPath.Text = "Add Path";
            this.btnAddPath.UseVisualStyleBackColor = true;
            this.btnAddPath.Click += new System.EventHandler(this.btnAddPath_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.lbSources);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 36);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(12);
            this.panel2.Size = new System.Drawing.Size(407, 321);
            this.panel2.TabIndex = 1;
            // 
            // lbSources
            // 
            this.lbSources.AllowDrop = true;
            this.lbSources.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbSources.FormattingEnabled = true;
            this.lbSources.Location = new System.Drawing.Point(12, 12);
            this.lbSources.Name = "lbSources";
            this.lbSources.Size = new System.Drawing.Size(383, 297);
            this.lbSources.TabIndex = 0;
            this.lbSources.DragDrop += new System.Windows.Forms.DragEventHandler(this.lbSources_DragDrop);
            this.lbSources.DragOver += new System.Windows.Forms.DragEventHandler(this.lbSources_DragOver);
            this.lbSources.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lbSources_KeyDown);
            this.lbSources.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lbSources_MouseDown);
            // 
            // ofd
            // 
            this.ofd.Filter = "All files|*.*";
            // 
            // PPSourcesPane
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "PPSourcesPane";
            this.Size = new System.Drawing.Size(407, 357);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnAddFile;
        private System.Windows.Forms.Button btnAddPath;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ListBox lbSources;
        private System.Windows.Forms.OpenFileDialog ofd;
    }
}
