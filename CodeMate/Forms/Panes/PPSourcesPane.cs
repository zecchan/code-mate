﻿using Microsoft.WindowsAPICodePack.Dialogs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CodeMate.Forms.Panes
{
    public partial class PPSourcesPane : UserControl
    {
        public PPSourcesPane()
        {
            InitializeComponent();
        }

        public string FileFilter { get; set; } = null;
        public List<string> Sources
        {
            get
            {
                var ls = new List<string>();
                foreach (var itm in lbSources.Items)
                {
                    ls.Add(itm.ToString());
                }
                return ls;
            }
            set {
                lbSources.Items.Clear();
                lbSources.Items.AddRange(value.ToArray());
            }
        }

        private void btnAddFile_Click(object sender, EventArgs e)
        {
            if (FileFilter != null)
                ofd.Filter = FileFilter;
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                var nsrc = Sources;
                nsrc.Add(ofd.FileName);
                Sources =nsrc;                
            }
        }

        private void btnAddPath_Click(object sender, EventArgs e)
        {
            CommonOpenFileDialog fbd = new CommonOpenFileDialog();
            fbd.IsFolderPicker = true;
            fbd.Title = "Select a folder to include";
            if (fbd.ShowDialog() == CommonFileDialogResult.Ok)
            {
                var nsrc = Sources;
                nsrc.Add(fbd.FileName);
                Sources = nsrc;
            }
        }

        private void lbSources_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Delete && lbSources.SelectedIndex >= 0)
            {
                if (MessageBox.Show("Delete selected item?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    lbSources.Items.RemoveAt(lbSources.SelectedIndex);
                }
            }
        }

        private void lbSources_MouseDown(object sender, MouseEventArgs e)
        {
            if (lbSources.SelectedItem == null) return;
            lbSources.DoDragDrop(lbSources.SelectedItem, DragDropEffects.Move);
        }

        private void lbSources_DragOver(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.Link;
            }
            else if (e.Data.GetDataPresent(DataFormats.StringFormat))
            {
                e.Effect = DragDropEffects.Move;
            }
        }

        private void lbSources_DragDrop(object sender, DragEventArgs e)
        {
            Point point = lbSources.PointToClient(new Point(e.X, e.Y));
            int index = lbSources.IndexFromPoint(point);
            if (index < 0) index = lbSources.Items.Count - 1;
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                var data = e.Data.GetData(DataFormats.FileDrop, true) as string[];
                if (data != null)
                {
                    foreach(var d in data)
                    {
                        if (!Sources.Any(x => x.ToLower().Trim() == d.ToLower().Trim()))
                            lbSources.Items.AddRange(data);
                    }
                }
            }
            else if (e.Data.GetDataPresent(DataFormats.StringFormat))
            {
                object data = e.Data.GetData(typeof(string));
                if (data != null)
                {
                    lbSources.Items.Remove(data);
                    lbSources.Items.Insert(index, data);
                }
            }
        }
    }
}
