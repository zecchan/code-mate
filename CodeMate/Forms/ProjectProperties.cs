﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CodeMate.Forms
{
    public partial class ProjectProperties : Form
    {
        private Project Project { get; set; }

        public ProjectProperties()
        {
            InitializeComponent();

            paneProject.Visible = false;
            paneProject.Dock = DockStyle.Fill;
            paneSources.Visible = false;
            paneSources.Dock = DockStyle.Fill;
            paneCompile.Visible = false;
            paneCompile.Dock = DockStyle.Fill;

            tvProps.ExpandAll();
            tvProps.SelectedNode = tvProps.Nodes[0];
        }

        public void LoadProject(Project project)
        {
            Project = project;
            paneProject.txName.Text = Project.Name;
            paneProject.txVersion.Text = Project.Template.Project.Version;
            paneProject.txOutput.Text = Project.Template.Project.Output;
            paneProject.cxAutoCompile.Checked = Project.Template.Project.AutoCompileOnChange;

            paneSources.Sources = Project.Template.Compile.Sources;

            paneCompile.CompilerGuid = Project.Template.Compile.Compiler;
        }

        private void SaveValues()
        {
            Project.Template.Project.Name = paneProject.txName.Text;
            Project.Template.Project.Version = paneProject.txVersion.Text;
            Project.Template.Project.Output = paneProject.txOutput.Text;
            Project.Template.Project.AutoCompileOnChange = paneProject.cxAutoCompile.Checked;

            Project.Template.Compile.Sources = paneSources.Sources;

            if (paneCompile.CompilerGuid.HasValue)
                Project.Template.Compile.Compiler = paneCompile.CompilerGuid.Value;
            else
                Project.Template.Compile.Compiler = Guid.Empty;
        }

        public Project GetProject()
        {
            return Project;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveValues();
            DialogResult = DialogResult.OK;
            Close();
        }

        private void tvProps_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (e.Node is TreeNode tn)
            {
                if (tn.Tag is string tag)
                {
                    paneProject.Visible = tag == "project";
                    paneSources.Visible = tag == "sources";
                    paneCompile.Visible = tag == "compile";
                }
            }
        }
    }
}
