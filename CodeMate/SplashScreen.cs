﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CodeMate
{
    public partial class SplashScreen : Form
    {
        const string compilerPluginsPath = @"plugins\compiler";
        bool allowClose = false;
        public SplashScreen()
        {
            InitializeComponent();
        }

        private void SplashScreen_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!allowClose)
            {
                e.Cancel = true;
            }
        }

        private void UpdateStatus(string text, double percentage)
        {
            if (InvokeRequired)
            {
                Invoke(new Action(() =>
                {
                    lbStatus.Text = text;
                    pbStatus.Value = (int)Math.Min(100, Math.Max(0, percentage));
                }));
            }
            else
            {
                lbStatus.Text = text;
                pbStatus.Value = (int)Math.Min(100, Math.Max(0, percentage));
            }
        }

        private async void SplashScreen_Shown(object sender, EventArgs e)
        {
            await Task.Run(() =>
            {
                try
                {
                    if (Directory.Exists(compilerPluginsPath))
                    {
                        UpdateStatus("Loading compiler plugins...", 0);
                        GlobalStatic.AssemblyLibrary.LoadCompilers(compilerPluginsPath);
                    }

                    UpdateStatus("Completed, running app...", 100);
                    Invoke(new Action(() =>
                    {
                        allowClose = true;
                        DialogResult = DialogResult.OK;
                        Close();
                    }));
                }
                catch(Exception ex)
                {
                    Invoke(new Action(() =>
                    {
                        MessageBox.Show($"Fatal error: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        allowClose = true;
                        DialogResult = DialogResult.Cancel;
                        Close();
                    }));
                }
            });
        }
    }
}
