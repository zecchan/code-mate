﻿using CodeMate.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CodeMate
{
    public partial class FMain : Form
    {
        string ProjectPath { get; set; }
        Project project { get; set; }

        bool HasUnsavedChanges { get; set; }
        List<FileSystemWatcher> Watchers { get; } = new List<FileSystemWatcher>();
        Queue<FileSystemWatcher> DisposeWatchers { get; } = new Queue<FileSystemWatcher>();

        public FMain()
        {
            InitializeComponent();
            Initialize();
        }

        void Log(string message, bool raw = false)
        {
            if (InvokeRequired)
            {
                Invoke(new Action(() =>
                {
                    string timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    var curselStart = txLog.SelectionStart;
                    var curselLength = txLog.SelectionLength;
                    if (raw)
                    {
                        txLog.AppendText(message + Environment.NewLine);
                    }
                    else
                    {
                        txLog.AppendText($"[{timestamp}] {message}" + Environment.NewLine);
                    }
                    txLog.Select(txLog.Text.Length, 0);
                    txLog.ScrollToCaret();
                    txLog.Select(curselStart, curselLength);
                }));
            }
            else
            {
                string timestamp = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                var curselStart = txLog.SelectionStart;
                var curselLength = txLog.SelectionLength;
                if (raw)
                {
                    txLog.AppendText(message + Environment.NewLine);
                }
                else
                {
                    txLog.AppendText($"[{timestamp}] {message}" + Environment.NewLine);
                }
                txLog.Select(txLog.Text.Length, 0);
                txLog.ScrollToCaret();
                txLog.Select(curselStart, curselLength);
            }
        }

        void ClearWatchers()
        {
            foreach (var fsw in Watchers)
            {
                fsw.EnableRaisingEvents = false;
                fsw.Changed -= FileChanged;
                DisposeWatchers.Enqueue(fsw);
            }
            Watchers.Clear();
        }

        void Initialize()
        {
            Text = GlobalStatic.Name + " v" + GlobalStatic.Version;
            project = null;
            HasUnsavedChanges = false;
            ProjectPath = null;
            txLog.Clear();
            ClearWatchers();

            btnSave.Enabled = btnCompile.Enabled = btnOpenDest.Enabled = btnProperties.Enabled = false;
        }

        void LoadProject(string filePath)
        {
            Initialize();
            project = new Project(filePath);
            LoadProject();
        }

        void LoadProject()
        {
            if (project != null)
            {
                Text = GlobalStatic.Name + " v" + GlobalStatic.Version + " - " + project.Name + (string.IsNullOrWhiteSpace(project.Template.Project.Version) ? "" : " v" + project.Template.Project.Version) + (HasUnsavedChanges ? "*" : "");
                btnSave.Enabled = btnCompile.Enabled = btnOpenDest.Enabled = btnProperties.Enabled = true;

                ClearWatchers();

                if (project.Template.Project.AutoCompileOnChange)
                {
                    var watchedPaths = new List<string>();
                    foreach (var src in project.Template.Compile.Sources)
                    {
                        var path = src;
                        if (File.Exists(path))
                            path = Path.GetDirectoryName(path);
                        watchedPaths.Add(Path.GetFullPath(path).ToLower());
                    }
                    watchedPaths = watchedPaths.Distinct().ToList();

                    foreach(var path in watchedPaths)
                    {
                        var fsw = new FileSystemWatcher();
                        fsw.Path = path;
                        fsw.IncludeSubdirectories = false;
                        fsw.NotifyFilter = NotifyFilters.LastWrite;
                        fsw.Changed += FileChanged;
                        fsw.EnableRaisingEvents = true;
                        Watchers.Add(fsw);
                    }
                }
            }
        }

        DateTime? requestCompile;
        private void FileChanged(object sender, FileSystemEventArgs e)
        {
            if (sender is FileSystemWatcher fsw && fsw.EnableRaisingEvents)
            {
                Log($"File '{e.FullPath}' has changed");
                requestCompile = DateTime.Now.AddSeconds(1);
            }
        }

        private void FMain_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                btnNew.PerformClick();
            }
            if (e.KeyCode == Keys.F2)
            {
                btnOpen.PerformClick();
            }
            if (e.KeyCode == Keys.F3)
            {
                btnProperties.PerformClick();
            }
            if (e.KeyCode == Keys.F5)
            {
                btnCompile.PerformClick();
            }
            if (e.KeyCode == Keys.F6)
            {
                btnOpenDest.PerformClick();
            }
            if (e.KeyCode == Keys.S && e.Control)
            {
                btnSave.PerformClick();
            }
        }

        bool CloseProject()
        {
            return true;
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            if (!CloseProject()) return;
            if (ofdProject.ShowDialog() == DialogResult.OK)
            {
                LoadProject(ofdProject.FileName);

                Log($"Project '{project.Name}' loaded");
                ProjectPath = ofdProject.FileName;
            }
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            if (!CloseProject()) return;
            if (sfdProject.ShowDialog() == DialogResult.OK)
            {
                Initialize();
                project = new Project();
                project.Template.Project.Name = Path.GetFileNameWithoutExtension(sfdProject.FileName);
                LoadProject();

                Log($"Project '{project.Name}' created");
                ProjectPath = sfdProject.FileName;

                btnProperties.PerformClick();
            }
        }

        private void btnProperties_Click(object sender, EventArgs e)
        {
            var pp = new ProjectProperties();
            pp.LoadProject(project);
            if (pp.ShowDialog() == DialogResult.OK)
            {
                HasUnsavedChanges = true;
                LoadProject();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            project.SaveTemplate(ProjectPath);

            Log($"Project '{project.Name}' saved");
            HasUnsavedChanges = false;
            LoadProject();
        }

        bool isAutoCompile = false;
        private void tmrWatcher_Tick(object sender, EventArgs e)
        {
            if (DisposeWatchers.Count > 0)
            {
                var watcher = DisposeWatchers.Dequeue();
                watcher.Dispose();
            }

            if (requestCompile.HasValue)
            {
                if (DateTime.Now >= requestCompile.Value)
                {
                    requestCompile = null;
                    Log($"Auto-compile triggered");
                    isAutoCompile = true;
                    btnCompile.PerformClick();
                }
            }
        }

        private void btnOpenDest_Click(object sender, EventArgs e)
        {
            if (Directory.Exists(project.Template.Project.Output))
            {
                var psi = new ProcessStartInfo()
                {
                    FileName = "explorer.exe",
                    Arguments = $"\"{project.Template.Project.Output}\"",
                };
                Process.Start(psi);
            }
        }

        private void btnCompile_Click(object sender, EventArgs e)
        {
            btnCompile.Enabled = false;
            try
            {
                var autoCompile = isAutoCompile;
                isAutoCompile = false;

                Log($"Compiling {project.Name} v{project.Version}...");

                var compiler = GlobalStatic.AssemblyLibrary.GetCompiler(project.Template.Compile.Compiler);
                if (compiler == null)
                {
                    Log($"Compiler with Guid '{project.Template.Compile.Compiler}' is not found");
                }

                project.AssemblyLibrary = GlobalStatic.AssemblyLibrary;
                var res = project.Compile(project.Template.Project.Output);

                foreach(var log in res.Logs)
                {
                    Log(log, true);
                }

                if (res.IsSuccess)
                {
                    Log($"Compiled successfully");
                }
                else
                {
                    Log($"Failed to compile: " + res.Message);
                }
            }
            finally
            {
                btnCompile.Enabled = true;
            }
        }
    }
}
