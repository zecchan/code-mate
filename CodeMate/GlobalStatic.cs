﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeMate
{
    internal static class GlobalStatic
    {
        public static string Name { get; } = "CodeMate";

        public static Version Version { get; } = new Version(1, 0, 0);

        public static string Copyright { get; } = "Copyright©2022 Code Atelier";

        public static AssemblyLibrary AssemblyLibrary { get; } = new AssemblyLibrary();

    }
}
