﻿using CodeMate.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeMate.Compiler.JavaScript
{
    public class JavaScriptCompiler : ICompiler
    {
        public Guid Guid { get; } = Guid.Parse("53274e09fee4422280b60adcdbb73929");

        public string Name { get; } = "CodeAtelier JavaScript Compiler";

        public Version Version { get; } = new Version(1, 0);

        public string Publisher { get; } = "CodeAtelier";

        public string Developer { get; } = "CodeAtelier";

        public CompileResult Compile(CompileTemplate compileTemplate, string outputPath)
        {
            var res = new CompileResult();

            try
            {
                var flist = new List<string>();
                var combinedBuffer = "";
                foreach (var file in compileTemplate.Sources)
                {
                    if (Directory.Exists(file))
                    {
                        var outFileName = Path.GetFileName(file) + ".js";
                        res.AddLog("Compiling '" + outFileName + "'...");
                        var files = Directory.EnumerateFiles(file);
                        var buffer = "";
                        foreach(var f in files)
                        {
                            var content = File.ReadAllText(f);
                            buffer += content + Environment.NewLine;
                            res.AddLog("Appended '" + Path.GetFileName(f) + "'");
                        }

                        var oPath = Path.Combine(outputPath, outFileName);
                        File.WriteAllText(oPath, buffer);
                    }
                    else if (File.Exists(file))
                    {
                        flist.Add(Path.GetFileName(file));
                        combinedBuffer += File.ReadAllText(file) + Environment.NewLine;
                    }
                    else
                    {
                        throw new FileNotFoundException("File not found", file);
                    }
                }
                if (!string.IsNullOrWhiteSpace(combinedBuffer))
                {
                    res.AddLog("Compiling 'compiled.js'...");
                    foreach (var f in flist)
                    {
                        res.AddLog("Appended '" + f + "'");
                    }
                    var oPath = Path.Combine(outputPath, "compiled.js");
                    File.WriteAllText(oPath, combinedBuffer);
                }
                res.IsSuccess = true;
            }
            catch(Exception ex) {
                res.AddLog("ERROR: " + ex.Message);
                res.Message = ex.Message;
                res.IsSuccess = false;
            }
            return res;
        }
    }
}
