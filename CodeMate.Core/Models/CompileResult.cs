﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeMate.Models
{
    public class CompileResult
    {
        public List<string> Logs { get; } = new List<string>();

        public void AddLog(string message)
        {
            Logs.Add(DateTime.Now.ToString("[yyyy-MM-dd HH:mm:ss] ") + message);
        }

        public bool IsSuccess { get; set; }

        public string Message { get; set; }
    }
}
