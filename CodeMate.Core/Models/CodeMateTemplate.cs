﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeMate.Models
{
    public class CodeMateTemplate
    {
        public ProjectTemplate Project { get; set; } = new ProjectTemplate();

        public CompileTemplate Compile { get; set; } = new CompileTemplate();
    }
}
