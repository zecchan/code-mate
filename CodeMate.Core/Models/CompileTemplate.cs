﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeMate.Models
{
    public class CompileTemplate
    {
        public Guid Compiler { get; set; } = Guid.Empty;

        public List<string> Sources { get; set; } = new List<string>();

        public Dictionary<string, string> Parameters { get; set; } = new Dictionary<string, string>();

        public EventsTemplate Events { get; set; } = new EventsTemplate();
    }
}
