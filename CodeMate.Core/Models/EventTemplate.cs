﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeMate.Models
{
    public class EventTemplate
    {
        public Guid? Handler { get; set; }

        public string Command { get; set; }

        public Dictionary<string, string> Parameters { get; set; }
    }
}
