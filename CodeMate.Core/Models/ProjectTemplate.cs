﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeMate.Models
{
    public class ProjectTemplate
    {
        public string Name { get; set; }

        public string Version { get; set; }

        public string Output { get; set; }

        public bool AutoCompileOnChange { get; set; }
    }
}
