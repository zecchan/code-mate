﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeMate.Models
{
    public class EventsTemplate
    {
        public EventTemplate PreBuild { get; set; } = new EventTemplate();

        public EventTemplate PostBuild { get; set; } = new EventTemplate();
    }
}
