﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CodeMate
{
    public class AssemblyLibrary : IAssemblyLibrary
    {
        private List<ICompiler> _compilerList = new List<ICompiler>();

        public ICompiler GetCompiler(Guid guid)
        {
            return _compilerList.FirstOrDefault(x => x.Guid == guid);
        }

        public List<ICompiler> GetCompilers()
        {
            return _compilerList.ToList();
        }

        public void LoadCompilers(string path)
        {
            var cmp = LoadTypesFromAssembly<ICompiler>(path);
            _compilerList.AddRange(cmp);
        }

        private IEnumerable<T> LoadTypesFromAssembly<T>(string path)
        {
            var res = new List<T>();
            if (Directory.Exists(path))
            {
                var files = Directory.GetFiles(path, "*.dll");
                foreach (var file in files)
                {
                    var cmpRes = LoadTypesFromAssembly<T>(file);
                    res.AddRange(cmpRes);
                }
            }
            else if (File.Exists(path))
            {
                path = Path.GetFullPath(path);
                var typ = typeof(T);
                var asm = Assembly.LoadFile(path);
                var exportedTypes = asm.GetExportedTypes().Where(x => typ.IsAssignableFrom(x));
                foreach(var exportedType in exportedTypes)
                {
                    var inst = Activator.CreateInstance(exportedType);
                    if (inst is T tinst)
                    {
                        res.Add(tinst);
                    }
                }
            }
            else
            {
                throw new FileNotFoundException($"'{path}' is not found");
            }
            return res;
        }
    }
}
