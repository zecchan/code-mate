﻿using CodeMate.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeMate
{
    public interface ICompiler
    {
        /// <summary>
        /// Gets the guid of this compiler
        /// </summary>
        Guid Guid { get; }

        /// <summary>
        /// Gets the name of this compiler
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets the version of this compiler
        /// </summary>
        Version Version { get; }

        /// <summary>
        /// Gets the publisher of this compiler
        /// </summary>
        string Publisher { get; }

        /// <summary>
        /// Gets the developer of this compiler
        /// </summary>
        string Developer { get; }

        /// <summary>
        /// Compiles a template to output path
        /// </summary>
        CompileResult Compile(CompileTemplate compileTemplate, string outputPath);
    }
}
