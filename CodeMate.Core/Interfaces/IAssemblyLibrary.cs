﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeMate
{
    public interface IAssemblyLibrary
    {
        void LoadCompilers(string path);

        ICompiler GetCompiler(Guid guid);
    }
}
