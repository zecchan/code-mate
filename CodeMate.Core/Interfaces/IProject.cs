﻿using CodeMate.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeMate
{
    public interface IProject
    {
        /// <summary>
        /// Gets the template loaded by this project
        /// </summary>
        CodeMateTemplate Template { get; }

        /// <summary>
        /// Compiles this project to output path
        /// </summary>
        CompileResult Compile(string outputPath);

        void LoadTemplate(string path);

        void SaveTemplate(string path);
    }
}
