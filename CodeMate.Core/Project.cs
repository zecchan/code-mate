﻿using CodeMate.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodeMate
{
    public class Project : IProject
    {
        public AssemblyLibrary AssemblyLibrary { get; set; }

        public Project()
        {
            Template = new CodeMateTemplate();
        }

        public Project(string templatePath)
        {
            LoadTemplate(templatePath);
        }

        public CodeMateTemplate Template { get; private set; }

        public string Name { get => Template?.Project?.Name; }
        public string Version { get => Template?.Project?.Version; }

        public CompileResult Compile(string outputPath)
        {
            if (AssemblyLibrary == null)
            {
                throw new ArgumentException(nameof(AssemblyLibrary) + " is null");
            }
            var compiler = AssemblyLibrary.GetCompiler(Template.Compile.Compiler);
            if (compiler == null)
            {
                throw new Exception($"Compiler with Guid '{Template.Compile.Compiler}' is not found");
            }

            var res = new CompileResult();
            res.IsSuccess = true;

            // handle prebuild event

            if (res.IsSuccess)
            {
                var cres = compiler.Compile(Template.Compile, outputPath);
                if (cres.IsSuccess == false)
                {
                    res.IsSuccess = false;
                    res.Message = cres.Message;
                }
                res.Logs.AddRange(cres.Logs);
            }

            // handle postbuild event

            return res;
        }

        public void LoadTemplate(string path)
        {
            var json = File.ReadAllText(path);
            Template = JsonConvert.DeserializeObject<CodeMateTemplate>(json);
        }

        public void SaveTemplate(string path)
        {
            var contractResolver = new DefaultContractResolver
            {
                NamingStrategy = new CamelCaseNamingStrategy(),
            };
            var json = JsonConvert.SerializeObject(Template, Formatting.None, new JsonSerializerSettings() { 
                ContractResolver = contractResolver,
                Formatting = Formatting.None
            });
            File.WriteAllText(path, json);
        }
    }
}
